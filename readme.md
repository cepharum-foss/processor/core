# forms-processor / core

## License

[MIT](LICENSE)

## Online manual

A dedicated online manual is available [here](https://cepharum-foss.gitlab.io/processor/core/).
