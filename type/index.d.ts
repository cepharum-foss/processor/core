import {ComputedRef} from "vue";

declare namespace FormsProcessor.Core {
    export interface Field {
        $required?: ComputedRef<boolean>
        $label: ComputedRef<string>
        /**
         *
         * @param input
         */
		validate(input: any): Promise<[ErrorMessage]>
    }
    export interface TextField extends Field {
        $placeholder?: ComputedRef<string>
    }
    export interface Context {
        l10n: {
            selectLocalized(raw: any): any
            loadMap(raw: any): Promise<any>
        }
    }
    export interface FieldDefinition {

    }
    export function FieldComposable<T>(field: Object, fieldDefinition: FieldDefinition, context: Context):T
    export type ErrorMessage = string | [string,...any]
}