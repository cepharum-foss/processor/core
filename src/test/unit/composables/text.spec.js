import { describe, it } from "vitest";
import { text } from "@/stores/composables/fields";
import "should";

const context = {
	l10n: {
		selectLocalized: v => v,
		translate: v => v,
		loadMap: () => undefined,
	}
};

describe( "the text composable", () => {
	it( "is a function", () => {
		text.should.be.a.Function();
	} );
	describe( "returns a validate function", () => {
		it( "it is a function", () => {
			const parsed = text( {},{}, context );
			parsed.validate.should.be.a.Function();
		} );
		it( "validates correctly if the property size is given", () => {
			text( {}, { size: "5-6" } ).validate( "foo" )
				.should.be.deepEqual( ["@VALIDATION.TOO_SHORT"] );
			text( {}, { size: "5-6 char" } ).validate( "foobar" )
				.should.be.deepEqual( [] );
			text( {}, { size: "1-3 chars" } ).validate( "foobar" )
				.should.be.deepEqual( ["@VALIDATION.TOO_LONG"] );
			text( {}, { size: "1-3 c" } ).validate( "foo" )
				.should.be.deepEqual( [] );
			text( {}, { size: "1-3" } ).validate( "fo" )
				.should.be.deepEqual( [] );
			text( {}, { size: "1-3" } ).validate( "f" )
				.should.be.deepEqual( [] );
			text( {}, { size: "2-3 words" } ).validate( "foo" )
				.should.be.deepEqual( ["@VALIDATION.TOO_SHORT"] );
			text( {}, { size: "2-3 words" } ).validate( "foo bar" )
				.should.be.deepEqual( [] );
			text( {}, { size: "2-3 words" } ).validate( "foo foo bar" )
				.should.be.deepEqual( [] );
			text( {}, { size: "1-2 words" } ).validate( "foo foo bar" )
				.should.be.deepEqual( ["@VALIDATION.TOO_LONG"] );
			text( {}, { size: "1-2 w" } ).validate( "foo bar" )
				.should.be.deepEqual( [] );
			text( {}, { size: "1-2 word" } ).validate( "foo" )
				.should.be.deepEqual( [] );
			text( {}, { size: [ "1-2 w", "-10 c" ] } ).validate( "foo" )
				.should.be.deepEqual( [] );
			text( {}, { size: [ 1,3 ] } ).validate( "foo" )
				.should.be.deepEqual( [] );
			text( {}, { size: [ 1,3 ] } ).validate( "foobar" )
				.should.be.deepEqual( ["@VALIDATION.TOO_LONG"] );
			text( {}, { size: [ 4,5 ] } ).validate( "foo" )
				.should.be.deepEqual( ["@VALIDATION.TOO_SHORT"] );
			text( {}, { size: [ "1-2 w", "-10 c" ] } ).validate( "foo bar" )
				.should.be.deepEqual( [] );
			text( {}, { size: [ "1-2 w", "-10 c" ] } ).validate( "foo ba ba" )
				.should.be.deepEqual( ["@VALIDATION.TOO_LONG"] );
			text( {}, { size: [ "1-2 w", "-10 c" ] } ).validate( "foofoobarbar" )
				.should.be.deepEqual( ["@VALIDATION.TOO_LONG"] );
		} );
	} );
	describe( "returns a normalize function", () => {
		it( "it is a function", () => {
			const parsed = text( {},{}, context );
			parsed.normalize.should.be.a.Function();
		} );
		it( "returns lowercase if the property lowercase is truthy", () => {
			text( {}, { lowercase: true }, context ).normalize( "FOO" ).should.equal( "foo" );
			text( {}, { lowercase: 1 }, context ).normalize( "FOO" ).should.equal( "foo" );
			text( {}, { lowercase: "yes" }, context ).normalize( "FOO" ).should.equal( "foo" );
			text( {}, { lowercase: "y" }, context ).normalize( "FOO" ).should.equal( "foo" );
		} );
		it( "returns uppercase if the property uppercase is truthy", () => {
			text( {}, { uppercase: true }, context ).normalize( "foo" ).should.equal( "FOO" );
			text( {}, { uppercase: 1 }, context ).normalize( "foo" ).should.equal( "FOO" );
			text( {}, { uppercase: "yes" }, context ).normalize( "foo" ).should.equal( "FOO" );
			text( {}, { uppercase: "y" }, context ).normalize( "foo" ).should.equal( "FOO" );
		} );
		it( "trims if the property normalization is set to trim", () => {
			text( {}, { normalization: "trim" }, context ).normalize( "   foo   " ).should.equal( "foo" );
			text( {}, { normalization: "trim" }, context ).normalize( "   foo" ).should.equal( "foo" );
			text( {}, { normalization: "trim" }, context ).normalize( "foo   " ).should.equal( "foo" );

			text( {}, { normalization: "Trim" }, context ).normalize( "   foo   " ).should.equal( "foo" );
			text( {}, { normalization: "Trim" }, context ).normalize( "   foo" ).should.equal( "foo" );
			text( {}, { normalization: "Trim" }, context ).normalize( "foo   " ).should.equal( "foo" );

			text( {}, { normalization: "trIm" }, context ).normalize( "   foo   " ).should.equal( "foo" );
			text( {}, { normalization: "trIm" }, context ).normalize( "   foo" ).should.equal( "foo" );
			text( {}, { normalization: "trIm" }, context ).normalize( "foo   " ).should.equal( "foo" );

			text( {}, { normalization: "TRIM" }, context ).normalize( "   foo   " ).should.equal( "foo" );
			text( {}, { normalization: "TRIM" }, context ).normalize( "   foo" ).should.equal( "foo" );
			text( {}, { normalization: "TRIM" }, context ).normalize( "foo   " ).should.equal( "foo" );
		} );
		it( "reduces if the property normalization is set to trim", () => {
			text( {}, { normalization: "reduce" }, context ).normalize( "   foo    bar  " ).should.equal( "foo bar" );
			text( {}, { normalization: "reduce" }, context ).normalize( "   foo bar" ).should.equal( "foo bar" );
			text( {}, { normalization: "reduce" }, context ).normalize( "foo bar  " ).should.equal( "foo bar" );

			text( {}, { normalization: "Reduce" }, context ).normalize( "   foo    bar  " ).should.equal( "foo bar" );
			text( {}, { normalization: "Reduce" }, context ).normalize( "   foo bar" ).should.equal( "foo bar" );
			text( {}, { normalization: "Reduce" }, context ).normalize( "foo bar  " ).should.equal( "foo bar" );

			text( {}, { normalization: "redUce" }, context ).normalize( "   foo    bar  " ).should.equal( "foo bar" );
			text( {}, { normalization: "redUce" }, context ).normalize( "   foo bar" ).should.equal( "foo bar" );
			text( {}, { normalization: "redUce" }, context ).normalize( "foo bar  " ).should.equal( "foo bar" );

			text( {}, { normalization: "REDUCE" }, context ).normalize( "   foo    bar  " ).should.equal( "foo bar" );
			text( {}, { normalization: "REDUCE" }, context ).normalize( "   foo bar" ).should.equal( "foo bar" );
			text( {}, { normalization: "REDUCE" }, context ).normalize( "foo bar  " ).should.equal( "foo bar" );
		} );
		it( "does nothing if the property normalization is set to none", () => {
			text( {}, { normalization: "none" }, context ).normalize( "   foo    bar  " ).should.equal( "   foo    bar  " );
			text( {}, { normalization: "none" }, context ).normalize( "   foo bar" ).should.equal( "   foo bar" );
			text( {}, { normalization: "none" }, context ).normalize( "foo bar  " ).should.equal( "foo bar  " );

			text( {}, { normalization: "None" }, context ).normalize( "   foo    bar  " ).should.equal( "   foo    bar  " );
			text( {}, { normalization: "None" }, context ).normalize( "   foo bar" ).should.equal( "   foo bar" );
			text( {}, { normalization: "None" }, context ).normalize( "foo bar  " ).should.equal( "foo bar  " );

			text( {}, { normalization: "noNe" }, context ).normalize( "   foo    bar  " ).should.equal( "   foo    bar  " );
			text( {}, { normalization: "noNe" }, context ).normalize( "   foo bar" ).should.equal( "   foo bar" );
			text( {}, { normalization: "noNe" }, context ).normalize( "foo bar  " ).should.equal( "foo bar  " );

			text( {}, { normalization: "NONE" }, context ).normalize( "   foo    bar  " ).should.equal( "   foo    bar  " );
			text( {}, { normalization: "NONE" }, context ).normalize( "   foo bar" ).should.equal( "   foo bar" );
			text( {}, { normalization: "NONE" }, context ).normalize( "foo bar  " ).should.equal( "foo bar  " );
		} );
		it( "returns prefix if a prefix is defined", () => {
			( text().$prefix === undefined ).should.be.true();
			text( {}, { prefix: "none" }, context ).$prefix.value.should.equal( "none" );
			text( {}, { prefix: "foo" }, context ).$prefix.value.should.equal( "foo" );
			text( {}, { prefix: "FOO" }, context ).$prefix.value.should.equal( "FOO" );
			text( {}, { prefix: "FoO" }, context ).$prefix.value.should.equal( "FoO" );
		} );
		it( "returns suffix if a suffix is defined", () => {
			( text().$suffix === undefined ).should.be.true();
			text( {}, { suffix: "none" }, context ).$suffix.value.should.equal( "none" );
			text( {}, { suffix: "foo" }, context ).$suffix.value.should.equal( "foo" );
			text( {}, { suffix: "FOO" }, context ).$suffix.value.should.equal( "FOO" );
			text( {}, { suffix: "FoO" }, context ).$suffix.value.should.equal( "FoO" );
		} );
	} );
	it( "parses the placeholder property correctly", () => {
		( text().$required === undefined ).should.be.true();
		const parsed1 = text( { },{ placeholder: "foo" }, context );
		parsed1.$placeholder.should.not.be.Undefined();
		parsed1.$placeholder.value.should.be.equal( "foo" );
	} );
	it( "parses the reducer property correctly", () => {
		( text().$required === undefined ).should.be.true();
		( text( { },{ reducer: null }, context ).$reducer === undefined ).should.be.true();
		text( { },{ reducer: "" }, context ).$reducer.should.not.be.Null();
		text( { },{ reducer: "/[\\s.-]/g" }, context ).$reducer.should.not.be.Undefined();
		text( { },{ reducer: "/[\\s.-]/g", size: "2-5" }, context )
			.validate( "foo" ).should.be.deepEqual( [] );
		text( { },{ reducer: "/[\\s.-]/g", size: "2-5" }, context )
			.validate( "   fo  o   " ).should.be.deepEqual( [] );
		text( { },{ reducer: "/[\\s.-]/g", size: "2-5" }, context )
			.validate( "   fo  o  o  o o  " ).should.be.deepEqual( ["@VALIDATION.TOO_LONG"] );
		text( { },{ reducer: "/[\\s.-]/g", size: "2-5" }, context )
			.validate( "   f     " ).should.be.deepEqual( ["@VALIDATION.TOO_SHORT"] );
	} );
	it( "parses the property regexp correctly", () => {
		( text().$regexp === undefined ).should.be.true();
		text( {}, { regexp: /^\d+$/ } ).$regexp.should.not.be.undefined();
		text( {}, { regexp: /^\d+$/ } ).validate( "123" ).should.be.deepEqual( [] );
		text( {}, { regexp: /^\d+$/ } ).validate( "123f" ).should.be.deepEqual( ["@VALIDATION.PATTERN_MISMATCH"] );
	} );
	it( "returns a format function", () => {
		text().format.should.be.a.Function();
		text().format( 1 ).should.equal( "1" );
		text().format( "foo" ).should.equal( "foo" );
	} );
} );
