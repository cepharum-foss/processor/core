import { describe, expect, it } from "vitest";
import { parseSequence } from "@/stores/helper/parser";
import { createPinia } from "pinia";

const context = {
	l10n: {
		selectLocalized: v => v,
		translate: v => v,
		loadMap: () => undefined,
	}
};

describe( "parser", () => {
	it( "is a function", () => {
		expect( typeof parseSequence ).to.be.eq( "function" );
	} );
	it( "returns a pinia useStore functions", () => {
		const parsed = parseSequence( [{
			name: "soleForm",
			label: "Sole Form",
			fields: [
				{
					name: "someField",
					label: "Some Field"
				}
			]
		}], context );
		expect( typeof parsed ).to.eq( "function" );
	} );
	describe( "correctly parses a form", () => {
		it( "parses the property name correctly", () => {
			const parsed = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
			}], context )( createPinia() );

			expect( parsed.soleform ).not.toBeUndefined();
			expect( parsed.soleform.$name ).not.toBeUndefined();
			expect( parsed.soleform.$name ).to.eq( "soleform" );

			const parsed1 = parseSequence( [{
				name: "soleform",
				label: "Sole Form",
			}], context )();

			expect( parsed1.soleform ).not.toBeUndefined();
			expect( parsed1.soleform.$name ).not.toBeUndefined();
			expect( parsed1.soleform.$name ).to.eq( "soleform" );

			const parsed2 = parseSequence( [{
				name: "SoleForm",
				label: "Sole Form",
			}], context )( createPinia() );

			expect( parsed2.soleform ).not.toBeUndefined();
			expect( parsed2.soleform.$name ).not.toBeUndefined();
			expect( parsed2.soleform.$name ).to.eq( "soleform" );

			const parsed3 = parseSequence( [{
				name: "SOLEFORM",
				label: "Sole Form",
			}], context )( createPinia() );

			expect( parsed3.soleform ).not.toBeUndefined();
			expect( parsed3.soleform.$name ).not.toBeUndefined();
			expect( parsed3.soleform.$name ).to.eq( "soleform" );
		} );
		it( "parses the property label correctly if a label and no title are given", () => {
			const parsed = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						label: "Some Field"
					}
				]
			}], context )( createPinia() );

			expect( parsed.soleform ).not.toBeUndefined();
			expect( parsed.soleform.$label ).not.toBeUndefined();
			expect( parsed.soleform.$label ).to.eq( "Sole Form" );
		} );
		it( "parses the property label correctly if a label and a title are given", () => {
			const parsed = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						label: "Some Field"
					}
				]
			}], context )( createPinia() );

			expect( parsed.soleform ).not.toBeUndefined();
			expect( parsed.soleform.$label ).not.toBeUndefined();
			expect( parsed.soleform.$label ).to.eq( "Sole Form" );
		} );
		it( "parses the property label correctly if no label and a title are given", () => {
			const parsedNoLabelWithTitle = parseSequence( [{
				name: "soleForm",
				title: "Sole Title",
				fields: [
					{
						name: "someField",
						label: "Some Field"
					}
				]
			}], context )( createPinia() );

			expect( parsedNoLabelWithTitle.soleform ).not.toBeUndefined();
			expect( parsedNoLabelWithTitle.soleform.$label ).not.toBeUndefined();
			expect( parsedNoLabelWithTitle.soleform.$label ).to.eq( "Sole Title" );
		} );
		it( "parses the property title correctly if a title and no label are given", () => {
			const parsed = parseSequence( [{
				name: "soleForm",
				title: "Sole Title",
				fields: [
					{
						name: "someField",
						label: "Some Field"
					}
				]
			}], context )( createPinia() );

			expect( parsed.soleform ).not.toBeUndefined();
			expect( parsed.soleform.$title ).not.toBeUndefined();
			expect( parsed.soleform.$title ).to.eq( "Sole Title" );
		} );
		it( "parses the property title correctly if a title and a label are given", () => {
			const parsed = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				title: "Sole Title",
				fields: [
					{
						name: "someField",
						label: "Some Field"
					}
				]
			}], context )( createPinia() );

			expect( parsed.soleform ).not.toBeUndefined();
			expect( parsed.soleform.$title ).not.toBeUndefined();
			expect( parsed.soleform.$title ).to.eq( "Sole Title" );
		} );
		it( "parses the property title correctly if no title is given", () => {
			const parsedNoTitle = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						label: "Some Field"
					}
				]
			}], context )( createPinia() );

			expect( parsedNoTitle.soleform ).not.toBeUndefined();
			expect( parsedNoTitle.soleform.$title ).not.toBeUndefined();
			expect( parsedNoTitle.soleform.$title ).to.eq( "Sole Form" );
		} );
		it( "parses the property description correctly", () => {
			const parsed = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				description: "Sole Description"
			}], context )( createPinia() );

			expect( parsed.soleform ).not.toBeUndefined();
			expect( parsed.soleform.$description ).not.toBeUndefined();
			expect( parsed.soleform.$description ).to.eq( "Sole Description" );
		} );
		it( "parses the property fields correctly", () => {
			const parsed = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						label: "Some Field"
					}
				]
			}], context )( createPinia() );

			expect( parsed.soleform ).not.toBeUndefined();
			expect( parsed.soleform.somefield ).not.toBeUndefined();
		} );
	} );
	describe( "correctly parses a field", () => {
		it( "parses the property type correctly if the type is text", () => {
			const parsed = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsed ).toHaveProperty( "soleform" );
			expect( parsed.soleform ).toHaveProperty( "somefield" );
			expect( parsed.soleform.somefield ).toHaveProperty( "$type" );
			expect( parsed.soleform.somefield.$type ).to.eq( "text" );
		} );
		it( "parses the property type correctly if the type is button", () => {
			const parsedTypeButton = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						type: "button"
					}
				]
			}], context )( createPinia() );

			expect( parsedTypeButton ).toHaveProperty( "soleform" );
			expect( parsedTypeButton.soleform ).toHaveProperty( "somefield" );
			expect( parsedTypeButton.soleform.somefield ).toHaveProperty( "$type" );
			expect( parsedTypeButton.soleform.somefield.$type ).to.eq( "button" );
		} );
		it( "parses the property type correctly if no value is given", () => {
			const parsedNoType = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
					}
				]
			}], context )( createPinia() );

			expect( parsedNoType ).toHaveProperty( "soleform" );
			expect( parsedNoType.soleform ).toHaveProperty( "somefield" );
			expect( parsedNoType.soleform.somefield ).toHaveProperty( "$type" );
			expect( parsedNoType.soleform.somefield.$type ).to.eq( "text" );
		} );
		it( "parses the property name correctly", () => {
			const parsed = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsed ).toHaveProperty( "soleform" );
			expect( parsed.soleform ).toHaveProperty( "somefield" );
			expect( parsed.soleform.somefield ).toHaveProperty( "$name" );
			expect( parsed.soleform.somefield.$name ).to.eq( "somefield" );
		} );
		it( "parses the property label correctly", () => {
			const parsed = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						label: "Some Field",
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsed.soleform ).not.toBeUndefined();
			expect( parsed.soleform.somefield ).not.toBeUndefined();
			expect( parsed.soleform.somefield.$label ).not.toBeUndefined();
			expect( parsed.soleform.somefield.$label ).to.eq( "Some Field" );
		} );
		it( "parses the property label correctly if no value is given", () => {
			const parsed = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsed.soleform ).not.toBeUndefined();
			expect( parsed.soleform.somefield ).not.toBeUndefined();
			expect( parsed.soleform.somefield.$label ).toBeUndefined();
		} );
		it( "parses the property required correctly if required is true", async() => {
			const parsed = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						label: "Some Field",
						required: true,
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsed.soleform ).not.toBeUndefined();
			expect( parsed.soleform.somefield ).not.toBeUndefined();
			expect( parsed.soleform.somefield.$required ).not.toBeUndefined();
			expect( parsed.soleform.somefield.$required ).toBeTruthy();
			expect( parsed.soleform.somefield.value ).eq( null );
			expect( parsed.soleform.somefield.$errorKeys ).to.deep.equal( [] );
			await parsed.soleform.somefield.setValue( "foo" );
			expect( parsed.soleform.somefield.value ).eq( "foo" );
			expect( parsed.soleform.somefield.$errorKeys ).to.deep.equal( [] );
			await parsed.soleform.somefield.setValue( "" );
			expect( parsed.soleform.somefield.value ).eq( "" );
			expect( parsed.soleform.somefield.$errorKeys ).to.deep.equal( ["@VALIDATION.MISSING_REQUIRED"] );
			await parsed.soleform.somefield.setValue( null );
			expect( parsed.soleform.somefield.value ).eq( null );
			expect( parsed.soleform.somefield.$errorKeys ).to.deep.equal( ["@VALIDATION.MISSING_REQUIRED"] );
			await parsed.soleform.somefield.setValue( "bar" );
			expect( parsed.soleform.somefield.value ).eq( "bar" );
			expect( parsed.soleform.somefield.$errorKeys ).to.deep.equal( [] );

		} );
		it( "parses the property required correctly if required is false", () => {
			const parsedNotRequired = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						label: "Some Field",
						required: false,
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsedNotRequired.soleform ).not.toBeUndefined();
			expect( parsedNotRequired.soleform.somefield ).not.toBeUndefined();
			expect( parsedNotRequired.soleform.somefield.$required ).not.toBeUndefined();
			expect( parsedNotRequired.soleform.somefield.$required ).toBeFalsy();
		} );
		it( "parses the property required correctly if no value is given", () => {
			const parsedNoRequired = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						label: "Some Field",
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsedNoRequired.soleform ).not.toBeUndefined();
			expect( parsedNoRequired.soleform.somefield ).not.toBeUndefined();
			expect( parsedNoRequired.soleform.somefield.$required ).toBeUndefined();
		} );
		it( "parses the property visible correctly if visible is true", () => {
			const parsed = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						visible: true,
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsed.soleform ).not.toBeUndefined();
			expect( parsed.soleform.somefield ).not.toBeUndefined();
			expect( parsed.soleform.somefield.$visible ).not.toBeUndefined();
			expect( parsed.soleform.somefield.$visible ).toBeTruthy();
		} );
		it( "parses the property visible correctly if visible is false", () => {
			const parsedNotVisible = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						visible: false,
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsedNotVisible.soleform ).not.toBeUndefined();
			expect( parsedNotVisible.soleform.somefield ).not.toBeUndefined();
			expect( parsedNotVisible.soleform.somefield.$visible ).not.toBeUndefined();
			expect( parsedNotVisible.soleform.somefield.$visible ).toBeFalsy();
		} );
		it( "parses the property visible correctly if no value is given", () => {
			const parsedNoVisible = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsedNoVisible.soleform ).not.toBeUndefined();
			expect( parsedNoVisible.soleform.somefield ).not.toBeUndefined();
			expect( parsedNoVisible.soleform.somefield.$visible ).toBeUndefined();
		} );
		it( "parses the property disabled correctly if disable is true", () => {
			const parsed = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						disabled: true,
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsed.soleform ).not.toBeUndefined();
			expect( parsed.soleform.somefield ).not.toBeUndefined();
			expect( parsed.soleform.somefield.$disabled ).not.toBeUndefined();
			expect( parsed.soleform.somefield.$disabled ).toBeTruthy();
		} );
		it( "parses the property disabled correctly if disabled is false", () => {
			const parsedNotDisabled = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						disabled: false,
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsedNotDisabled.soleform ).not.toBeUndefined();
			expect( parsedNotDisabled.soleform.somefield ).not.toBeUndefined();
			expect( parsedNotDisabled.soleform.somefield.$disabled ).not.toBeUndefined();
			expect( parsedNotDisabled.soleform.somefield.$disabled ).toBeFalsy();

		} );
		it( "parses the property disabled correctly if no value is given", () => {
			const parsedNoDisabled = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsedNoDisabled.soleform ).not.toBeUndefined();
			expect( parsedNoDisabled.soleform.somefield ).not.toBeUndefined();
			expect( parsedNoDisabled.soleform.somefield.$disabled ).toBeUndefined();
		} );
		it( "parses the property initial correctly if initial is a Boolean", () => {
			const parsedInitialBoolean = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						initial: true,
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsedInitialBoolean.soleform ).not.toBeUndefined();
			expect( parsedInitialBoolean.soleform.somefield ).not.toBeUndefined();
			expect( parsedInitialBoolean.soleform.somefield.$initial ).not.toBeUndefined();
			expect( parsedInitialBoolean.soleform.somefield.$initial ).toBeTruthy();
		} );
		it( "parses the property initial correctly if initial is a Number", () => {
			const parsedInitialNumber = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						initial: 1,
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsedInitialNumber.soleform ).not.toBeUndefined();
			expect( parsedInitialNumber.soleform.somefield ).not.toBeUndefined();
			expect( parsedInitialNumber.soleform.somefield.$initial ).not.toBeUndefined();
			expect( parsedInitialNumber.soleform.somefield.$initial ).to.be.eq( 1 );
		} );
		it( "parses the property initial correctly if initial is a String", () => {
			const parsedInitialString = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						initial: "someValue",
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsedInitialString.soleform ).not.toBeUndefined();
			expect( parsedInitialString.soleform.somefield ).not.toBeUndefined();
			expect( parsedInitialString.soleform.somefield.$initial ).not.toBeUndefined();
			expect( parsedInitialString.soleform.somefield.$initial ).to.be.eq( "someValue" );
		} );
		it( "parses the property initial correctly if no value is Given", () => {
			const parsedNoVisible = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsedNoVisible.soleform ).not.toBeUndefined();
			expect( parsedNoVisible.soleform.somefield ).not.toBeUndefined();
			expect( parsedNoVisible.soleform.somefield.$initial ).toBeUndefined();
		} );
		it( "parses the property messages correctly", () => {
			const parsedMessage = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						messages: "Some Message",
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsedMessage.soleform ).not.toBeUndefined();
			expect( parsedMessage.soleform.somefield ).not.toBeUndefined();
			expect( parsedMessage.soleform.somefield.$messages ).not.toBeUndefined();
			expect( parsedMessage.soleform.somefield.$messages ).to.be.eq( "Some Message" );
		} );
		it( "parses the property messages correctly if no value is given", () => {
			const parsedNoMessage = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsedNoMessage.soleform ).not.toBeUndefined();
			expect( parsedNoMessage.soleform.somefield ).not.toBeUndefined();
			expect( parsedNoMessage.soleform.somefield.$messages ).toBeUndefined();
		} );
		it( "parses the property classes correctly", () => {
			const parsedClasses = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						classes: "Some Class",
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsedClasses.soleform ).not.toBeUndefined();
			expect( parsedClasses.soleform.somefield ).not.toBeUndefined();
			expect( parsedClasses.soleform.somefield.$classes ).not.toBeUndefined();
			expect( parsedClasses.soleform.somefield.$classes ).to.be.eq( "Some Class" );
		} );
		it( "parses the property classes correctly if no value is given", () => {
			const parsedNoClasses = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsedNoClasses.soleform ).not.toBeUndefined();
			expect( parsedNoClasses.soleform.somefield ).not.toBeUndefined();
			expect( parsedNoClasses.soleform.somefield.$classes ).toBeUndefined();
		} );
		it( "parses the property hint correctly", () => {
			const parsedHint = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						hint: "Some Hint",
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsedHint.soleform ).not.toBeUndefined();
			expect( parsedHint.soleform.somefield ).not.toBeUndefined();
			expect( parsedHint.soleform.somefield.$hint ).not.toBeUndefined();
			expect( parsedHint.soleform.somefield.$hint ).to.be.eq( "Some Hint" );
		} );
		it( "parses the property hint correctly if no value is given", () => {
			const parsedNoHint = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsedNoHint.soleform ).not.toBeUndefined();
			expect( parsedNoHint.soleform.somefield ).not.toBeUndefined();
			expect( parsedNoHint.soleform.somefield.$hint ).toBeUndefined();
		} );
		it( "parses the property markdown correctly", () => {
			const parsedMarkdown = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						markdown: true,
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsedMarkdown.soleform ).not.toBeUndefined();
			expect( parsedMarkdown.soleform.somefield ).not.toBeUndefined();
			expect( parsedMarkdown.soleform.somefield.$markdown ).not.toBeUndefined();
			expect( parsedMarkdown.soleform.somefield.$markdown ).toBeTruthy();
		} );
		it( "parses the property markdown correctly if markdown is false", () => {
			const parsedNotMarkdown = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						markdown: false,
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsedNotMarkdown.soleform ).not.toBeUndefined();
			expect( parsedNotMarkdown.soleform.somefield ).not.toBeUndefined();
			expect( parsedNotMarkdown.soleform.somefield.$markdown ).not.toBeUndefined();
			expect( parsedNotMarkdown.soleform.somefield.$markdown ).toBeFalsy();
		} );
		it( "parses the property markdown correctly if no value is given", () => {
			const parsedNoMarkdown = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						type: "text"
					}
				]
			}], context )( createPinia() );

			expect( parsedNoMarkdown.soleform ).not.toBeUndefined();
			expect( parsedNoMarkdown.soleform.somefield ).not.toBeUndefined();
			expect( parsedNoMarkdown.soleform.somefield.$markdown ).toBeUndefined();
		} );
		it( "parses the property description correctly if no value is given", () => {
			const parsedNoMarkdown = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
					}
				]
			}], context )( createPinia() );

			expect( parsedNoMarkdown.soleform ).not.toBeUndefined();
			expect( parsedNoMarkdown.soleform.somefield ).not.toBeUndefined();
			expect( parsedNoMarkdown.soleform.somefield.$description ).toBeUndefined();
		} );
		it( "parses the property description correctly if a value is given", () => {
			const parsedNoMarkdown = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
						description: "foo",
					}
				]
			}], context )( createPinia() );

			expect( parsedNoMarkdown.soleform ).not.toBeUndefined();
			expect( parsedNoMarkdown.soleform.somefield ).not.toBeUndefined();
			expect( parsedNoMarkdown.soleform.somefield.$description ).eq( "foo" );
		} );
		it( "has a property pristine that is initially true but turns false once the value is updated", () => {
			const parsedNoMarkdown = parseSequence( [{
				name: "soleForm",
				label: "Sole Form",
				fields: [
					{
						name: "someField",
					}
				]
			}], context )( createPinia() );

			expect( parsedNoMarkdown.soleform ).not.toBeUndefined();
			expect( parsedNoMarkdown.soleform.somefield ).not.toBeUndefined();
			expect( parsedNoMarkdown.soleform.somefield.$pristine ).eq( true );
			expect( parsedNoMarkdown.soleform.somefield.value ).eq( null );
			parsedNoMarkdown.soleform.somefield.setValue( "foo" );
			expect( parsedNoMarkdown.soleform.somefield.$pristine ).eq( false );
			expect( parsedNoMarkdown.soleform.somefield.value ).eq( "foo" );
		} );
	} );
} );
