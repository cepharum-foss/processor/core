import { describe, expect, it } from "vitest";
import { createRegistry } from "@/lib/registry";

describe( "the registry", () => {
	it( "is a function, that returns an Object", () => {
		expect( typeof createRegistry ).to.be.eq( "function" );
		expect( typeof createRegistry() ).to.be.eq( "object" );
	} );
	it( "object has the property register that is a function", () => {
		expect( typeof createRegistry().register ).to.be.eq( "function" );
	} );
	it( "object has the property registerMultiple that is a function", () => {
		expect( typeof createRegistry().registerMultiple ).to.be.eq( "function" );
	} );
	it( "object has the property getForType that is a function", () => {
		expect( typeof createRegistry().getForType ).to.be.eq( "function" );
	} );
	it( "object will register default types", () => {
		const registry = createRegistry();
		expect( registry.getForType( "text" ) ).to.not.eq( undefined );
		expect( typeof registry.getForType( "text" ) ).to.eq( "function" );
	} );
	it( "object will not register default types if option empty is set true", () => {
		const registry = createRegistry( undefined,{ empty: true } );
		expect( registry.getForType( "text" ) ).to.eq( undefined );
	} );
	it( "object will register provided composables", () => {
		const registry = createRegistry( {
			foo: { name: "foo" },
			bar: { name: "bar" },
		} );
		expect( registry.getForType( "foo" ).name ).to.eq( "foo" );
		expect( registry.getForType( "bar" ).name ).to.eq( "bar" );
	} );
	it( "object will register provided composables and not register default types if option empty is set true", () => {
		const registry = createRegistry( {
			foo: { name: "foo" },
			bar: { name: "bar" },
		}, { empty: true } );
		expect( registry.getForType( "foo" ).name ).to.eq( "foo" );
		expect( registry.getForType( "bar" ).name ).to.eq( "bar" );
		expect( registry.getForType( "text" ) ).to.eq( undefined );
	} );
	it( "register function can be used to register a composable for a type", () => {
		const registry = createRegistry();
		registry.register( "foo", { name: "foo" } );
		expect( registry.getForType( "foo" ).name ).to.eq( "foo" );
	} );
	it( "registerMultiple function can be used to register Objects for a type", () => {
		const registry = createRegistry();
		registry.registerMultiple( {
			foo: { name: "foo" },
			bar: { name: "bar" },
		} );
		expect( registry.getForType( "foo" ).name ).to.eq( "foo" );
		expect( registry.getForType( "bar" ).name ).to.eq( "bar" );
	} );
	it( "getForType function can be used receive previously registered Objects", () => {
		const registry = createRegistry();
		expect( registry.getForType( "foo" ) ).to.eq( undefined );
		registry.register( "foo", { name: "foo" } );
		expect( registry.getForType( "foo" ).name ).to.eq( "foo" );
	} );
} );
