import { createRegistry } from "@/lib/registry";
import { parseSequence } from "@/stores/helper/parser";
import { getActivePinia } from "pinia";
import { l10n } from "@/stores/composables/l10n";

export function parseFormDefinition( form, options = {} ) {
	const context = options.context || {};
	context.registry = options.registry || createRegistry( options.types );
	context.id = options.id || "formsprocessor";
	const parsed = parseSequence( form.sequence, context );
	return pinia => {
		let _pinia = pinia;
		if ( _pinia == null ) {
			_pinia = getActivePinia();
		}
		context.l10n = l10n( context )( _pinia );
		return parsed( _pinia );
	};
}