import * as fields from "../stores/composables/fields/index";

/**
 * creates a registry and returns 3 functions that to work with that registry
 * @param {{}} composables composables that shall be added initially to the registry
 * @param {Boolean} empty true if the registry should contain all default type composables
 * @returns {{register: function, registerMultiple: function, getForType:function}} functions for interacting with the registry
 */
export function createRegistry( composables = {}, { empty = false } = {} ) {
	const registry = {};

	if ( !empty ) {
		registerMultiple( fields );
	}
	registerMultiple( composables );

	/**
	 * registers a composable for the provided type
	 * @param {String} type type for the composable, the registration is case non-sensitive
	 * @param {function} composable vue composable that will be used to contstuct the field store
	 * @returns {void} void
	 */
	function register( type, composable ) {
		if ( typeof type != "string" ) {
			throw new TypeError( "tried to register a type that is not a string" );
		}
		if ( registry[type.toLowerCase()] != null ) {
			console.warn( `overwriting a composable registered for type ${type}` );
		}
		registry[type.toLowerCase()] = composable;
	}

	/**
	 * registers a composable for the provided type
	 * @param {{}} FieldComposables vue composable that will be used to contstuct the field store
	 * @returns {void} void
	 */
	function registerMultiple( FieldComposables = {} ) {
		for ( const key of Object.keys( FieldComposables ) ) {
			register( key, FieldComposables[key] );
		}
	}

	/**
	 * returns a previous registered composable for the type
	 * @param {String} type type identifier for the composable
	 * @returns {{}} FieldComposable
	 */
	function getForType( type ) {
		if ( typeof type != "string" ) {
			throw new TypeError( "tried to register a type that is not a string" );
		}
		return registry[type.toLowerCase()];
	}

	return {
		register,
		registerMultiple,
		getForType,
	};
}
