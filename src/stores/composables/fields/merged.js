export function useMergedField( fieldComposables, context ) {
	let field = {};

	for ( const composable of fieldComposables ) {
		if ( composable != null ) {
			field = { ...field, ...composable };
		}
	}

	return {
		...field,
		async validate( value ) {
			let errors = [];
			const promises = [];

			for ( const composable of fieldComposables ) {
				if ( composable != null && composable.validate ) {
					promises.push( composable.validate( value ) );
				}
			}

			const errorList = await Promise.all( promises );

			for ( const list of errorList ) {
				if ( Array.isArray( list ) ) {
					errors = errors.concat( list );
				}
			}

			return errors;
		},
		normalize: function( value ) {
			let normalized = value;

			for ( const composable of fieldComposables ) {
				if ( composable != null && composable.normalize ) {
					normalized = composable.normalize( normalized );
				}
			}

			return normalized;
		},
		format: function( value ) {
			let formatted = value;

			for ( const composable of fieldComposables ) {
				if ( composable != null && composable.format ) {
					formatted = composable.normalize( formatted );
				}
			}

			return formatted;
		}
	};
}
