import { computed, ref, shallowRef } from "vue";
import Data from "@/service/data";
const { normalizeToBoolean } = Data;


/**
 * composable for a fieldStore
 * @param {store} fieldStore Store Object that will be returned from by the setup store for each field
 * @param {Object} fieldDefinition fieldDefinition provided in the FormsDefinitions Sequence
 * @param {Object} context context
 * @param {({validate: promise, format: function, normalize: function}|function)} typedComposable composable for the field type
 * @returns {{}} composable
 */
export const useBaseField = ( fieldStore, fieldDefinition, context = {} ) => {
	const { description, required, visible, disabled, initial, messages, classes, hint, markdown } = fieldDefinition;

	const errors = shallowRef( [] );

	return {
		$formattedValue: ref( null ),
		$pristine: ref( true ),
		$errorKeys: errors,
		$errors: computed( () => errors.value.map( error => {
			if ( Array.isArray( error ) ) {
				return context.l10n.translate( ...error );
			}
			if ( typeof error === "string" && error[0] === "@" ) {
				return context.l10n.translate( error.slice( 1 ) );
			}
			return error;
		} ) ),
		$description: description == null ? undefined : computed( () => description ),
		$required: required == null ? undefined : computed( () => normalizeToBoolean( required ) ),
		$visible: visible == null ? undefined : computed( () => normalizeToBoolean( visible ) ),
		$disabled: disabled == null ? undefined : computed( () => normalizeToBoolean( disabled ) ),
		$initial: initial == null ? undefined : computed( () => initial ),
		$messages: messages == null ? undefined : computed( () => messages ),
		$classes: classes == null ? undefined : computed( () => classes ),
		$hint: hint == null ? undefined : computed( () => hint ),
		$markdown: markdown == null ? undefined : computed( () => markdown ),
		$type: computed( () => fieldDefinition.type || "text" ),
		$valid: computed( () => errors.value?.length === 0 ),
		setValue: async function( value ) {
			this.$pristine = false;
			this.value = this.normalize( value );
			this.$formattedValue = this.format( this.value );
			errors.value = await this.validate( this.value );
		},
		validate: function( value ) {
			const _errors = [];
			if ( this.$required && ( value == null || value === "" ) ) {
				_errors.push( "@VALIDATION.MISSING_REQUIRED" );
			}
			return _errors;
		},
	};
};
