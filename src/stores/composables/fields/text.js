import { computed, unref } from "vue";
import { Range } from "@/utility/range";
import Data from "@/service/data";

const ptnPatternSyntax = /^\s*\/(.+)\/([gi]?)\s*$/i;

const sizeUnits = {
	char: [ "c", "character", "characters", "char", "chars" ],
	word: [ "w", "word", "words" ],
};

export function text( field = {}, fieldDefinition = {}, context = {} ) {
	const fieldObject = {};
	const { placeholder, prefix, suffix, reducer, regexp, size } = fieldDefinition;

	return {
		...fieldObject,
		$placeholder: placeholder == null ? undefined : computed( () => placeholder ),
		$prefix: prefix == null ? undefined : computed( () => prefix ),
		$suffix: suffix == null ? undefined : computed( () => suffix ),
		$regexp: regexp == null ? undefined : computed( () => regexp ),
		$reducer: reducer == null ? undefined : computed( () => extractReducer( reducer ) ),
		$range: size == null ? undefined : computed( () => extractRange( size ) ),

		normalize( value ) {
			if ( value == null ) return null;
			let _value = String( value );
			if ( Data.normalizeToBoolean( fieldDefinition.lowercase ) ) {
				_value = _value.toLowerCase();
			}
			if ( Data.normalizeToBoolean( fieldDefinition.uppercase ) ) {
				_value = _value.toUpperCase();
			}
			if ( fieldDefinition.normalization ) {
				switch ( fieldDefinition.normalization?.trim()?.toLowerCase() || "trim" ) {
					case "reduce" :
						_value = _value.trim();
						_value = _value.replace( / +(?= )/g,"" );
						break;
					case "trim" :
						_value = _value.trim();
						break;
					case "none" :
				}
			}
			return String( _value );
		},
		validate( input ) {
			let value = input;
			const errors = [];
			const _reducer = unref( this.$reducer );

			if ( _reducer ) {
				// https://stackoverflow.com/a/16046903/3182819
				const numCaptures = new RegExp( _reducer.source + "|" ).exec( "" ).length - 1;

				value = value.replace( _reducer, ( _, ...captures ) => captures.slice( 0, numCaptures ).join( "" ) );
			}

			const range = unref( this.$range );

			if ( range?.char ) {
				if ( range.char.isBelowRange( value.length ) ) {
					errors.push( "@VALIDATION.TOO_SHORT" );
				}
				if ( range.char.isAboveRange( value.length ) ) {
					errors.push( "@VALIDATION.TOO_LONG" );
				}
			}

			if ( range?.word ) {
				const numWords = value.trim().split( /\s+/ ).length;

				if ( range.word.isBelowRange( numWords ) ) {
					errors.push( "@VALIDATION.TOO_SHORT" );
				}

				if ( range.word.isAboveRange( numWords ) ) {
					errors.push( "@VALIDATION.TOO_LONG" );
				}
			}

			const _regexp = unref( this.$regexp );

			if ( _regexp && !_regexp.test( value ) ) {
				errors.push( "@VALIDATION.PATTERN_MISMATCH" );
			}

			return errors;
		},
		format: value => String( value ),
	};
}

/**
 * extract regExp reducer from rawValue
 * @param {null|String} rawValue rawValue for regExp as provided in fieldDefinition
 * @returns {RegExp|null} regexp that can be used to reduce
 */
function extractReducer( rawValue ) {
	if ( rawValue == null ) {
		return null;
	}

	const stringValue = String( rawValue ).trim();
	if ( stringValue === "" ) {
		return null;
	}

	const match = ptnPatternSyntax.exec( stringValue );
	if ( match ) {
		return new RegExp( match[1], match[2] );
	}

	return new RegExp( stringValue );
}

function extractRange( rawValue ) {
	if ( Array.isArray( rawValue ) && rawValue.every( item => !isNaN( Number( item ) ) && Number.isInteger( Number( item ) ) && Number( item ) >= 0 ) ) {
		return {
			char: new Range( rawValue )
		};
	}

	const range = {};
	const unitNames = Object.keys( sizeUnits );

	if ( rawValue.length || typeof rawValue === "string" ) {
		const _sizes = Array.isArray( rawValue ) ? rawValue : [rawValue];

		for ( const _size of _sizes ) {
			const matches = /^\s*(\S+)\s+(\w+)\s*$/.exec( _size );

			if ( matches ) {
				// The current option ends with a word.
				// Check if word is a known unit:
				const [ , rawSize, unit ] = matches;
				const unitName = unitNames.find( i => sizeUnits[i].includes( unit ) );

				if ( unitName ) {
					range[unitName] = new Range( rawSize );
					continue;
				}
			}

			range.char = new Range( _size );
		}
	}

	return range;
}
