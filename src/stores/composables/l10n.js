import { Localization, normalizeLocale } from "@/service/l10n";
import { ref, unref, watchEffect } from "vue";
import { defineStore } from "pinia";

// eslint-disable-next-line require-jsdoc
export function l10n( context ) {
	return defineStore( String( context.id || "formsprocessor" ) + ".l10n", () => {
		const locale = ref( normalizeLocale( context.locale ) );

		const map = ref();

		const loadMap = async( _locale = locale ) => {
			map.value = await Localization.loadMap( unref( _locale ) );
		};

		watchEffect( () => {
			loadMap( locale ); // eslint-disable-line promise/catch-or-return
		} );

		return {
			locale,
			map,
			translate: ( lookup, ...args ) => Localization.translate( map.value, lookup, ...args ),
			selectLocalized: ( value, desiredLocale = locale ) => Localization.selectLocalized( value, unref( desiredLocale ) ),
			setLocale: ( _locale = locale ) => {
				this.locale = normalizeLocale( unref( _locale ) );
			},
			loadMap,
		};
	} );

}
