import { computed, reactive, ref } from "vue";
import { defineStore } from "pinia";
import { useBaseField } from "@/stores/composables/fields/base";
import { Compiler, Interpolation } from "simple-terms";
import { createRegistry } from "@/lib/registry";
import { useMergedField } from "@/stores/composables/fields/merged";

export function parseSequence( form, context = {} ) {
	return defineStore( ( context.id || "formsprocessor" ) + ".root", () => {
		const field = {
			$scheme: computed( () => ( { name: "root", fields: form } ) ),
			$name: computed( () => "root" ),
		};
		const _root = field;
		const history = [];
		const formStores = form.map( store => parseForm( store, _root, history, context )() );
		for ( const store of formStores ) {
			const name = store.$name.toLowerCase();
			field[name] = computed( () => store );
		}
		return field;
	} );
}

export function parseForm( form, root, _history, context ) {
	const { fields, title, label } = form;
	const { l10n } = context;
	let { name: _name } = form;
	_name = _name == null ? _name : _name.toLowerCase();
	const subId = [ ..._history, form ].map( field => {
		return ( field.$scheme?.value.name || field?.name || "" ).toLowerCase();
	} ).join( "." );

	return defineStore( `${context.id || "formsprocessor"}.${subId}`, () => {
		if ( !label && !title ) {
			throw new Error( "invalid formDefinition: a field needs a label or a title" );
		}

		const field = {
			$scheme: computed( () => form ),
			$name: computed( () => _name ),
			$title: computed( () => ( title ? l10n.selectLocalized( title ) : field.$label.value ) ),
			$label: computed( () => ( label ? l10n.selectLocalized( label ) : field.$title.value ) ),
		};

		if ( fields != null ) {
			const fieldStores = fields.map( store => parseField( store, root, [ ..._history, field ], context )() );
			for ( const store of fieldStores ) {
				const name = ( store.$scheme.name || store.name ).toLowerCase();
				field[name] = computed( () => store );
			}
		}

		if ( form.description ) {
			field.$description = computed( () => form.description );
		}

		return field;
	} );
}

function parseField( fieldDefinition, root, _history, context ) {
	const { l10n } = context;
	const { label } = fieldDefinition;
	let { name: _name, } = fieldDefinition;
	_name = _name == null ? _name : _name.toLowerCase();
	const subId = [ ..._history, fieldDefinition ].map( field => {
		return ( field.$scheme?.value.name || field?.name || "" ).toLowerCase();
	} ).join( "." );

	return defineStore( `${context.id || "formsprocessor"}.${subId}`, () => {
		const field = {
			$scheme: computed( () => fieldDefinition ),
			$name: computed( () => _name ),
			$pristine: ref( true ),
			$touched: ref( false ),
			$label: computed( () => l10n.selectLocalized( label ) ),
		};
		if ( !fieldDefinition.fields || fieldDefinition.value != null ) {
			field.value = extractValue( fieldDefinition.value, root, field );
		}
		const preComputedDefinition = preComputeFieldDefinition( fieldDefinition, context );

		let typedField = ( context.registry || createRegistry() ).getForType( fieldDefinition.type || "" );
		if ( typeof typedField === "function" ) typedField = typedField( field, preComputedDefinition, context );
		const baseField = useBaseField( field, preComputedDefinition, context );

		return useMergedField( [ typedField, baseField, field ], context );
	} );
}

function preComputeFieldDefinition( fieldDefinition, context ) {
	const { l10n } = context;
	const processedDefinition = {};
	for ( const prop of Object.keys( fieldDefinition ) ) {
		if ( prop != "name" ) {
			processedDefinition[prop] = computed( () => l10n.selectLocalized( fieldDefinition[prop] ) );
		}
	}
	return reactive( processedDefinition );
}

function extractValue( value, root ) {
	if ( value == null ) {
		return ref( null );
	}
	if ( typeof value === "string" && value[0] === "=" ) {
		const fn = Compiler.compile( value.slice( 1 ), null, null, qualifiers => qualifier( root, qualifiers ) );
		return computed( () => fn( root ) );
	}
	const fn = Interpolation.parse( value, null, {
		keepLiteralString: true,
		asFunction: true,
		qualifier: qualifiers => qualifier( root, qualifiers )
	} );
	if ( typeof fn === "function" ) {
		return computed( () => fn( root ) );
	}
	return computed( () => fn );
}

function qualifier( root, qualifiers ) {
	return qualify( root, qualifiers.reverse() )[0].slice( 2 );
}

function qualify( current, qualifiers ) {
	const name = ( current.$scheme?.value?.name || current.name ).toLowerCase();
	const fields = current.$scheme?.value?.fields || current.fields;
	if ( fields ) {
		for ( const field of fields ) {
			const [ qualification, leftoverQualifiers ] = qualify( field, qualifiers );
			if ( qualification && ( leftoverQualifiers.length === 0 || leftoverQualifiers[0] === name.toLowerCase() ) ) {
				return [ [ name, "value", ...qualification ], leftoverQualifiers.slice( 1 ) ];
			}
		}
	}
	if ( name.toLowerCase() === qualifiers[0] ) {
		return [ [ name, "value" ], qualifiers.slice( 1 ) ];
	}
	return [ false, qualifiers ];
}
