import { createRouter, createWebHistory } from "vue-router";
import ProcessorView from "@/views/ProcessorView.vue";

const router = createRouter( {
	history: createWebHistory( import.meta.env.BASE_URL ),
	routes: [
		{
			path: "/",
			name: "home",
			redirect: "/processor/most-simple"
		},
		{
			path: "/processor/:route",
			name: "processorParameter",
			component: ProcessorView
		},
	]
} );

export default router;
