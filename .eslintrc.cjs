/* eslint-env node */
module.exports = {
    root: true,
    'extends': [
        'plugin:vue/vue3-essential',
        "eslint-config-cepharum"
    ],
    parserOptions: {
        ecmaVersion: 'latest'
    },
    rules: {
        "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
        "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
        "require-jsdoc": "warn",
        "vue/no-textarea-mustache": "warn",
    }
}
