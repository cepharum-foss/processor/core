---
layout: home

hero:
  name: Forms processor
  text: core
  tagline: a single library, endless use cases
  actions:
    - text: Getting started
      link: tutorial/getting-started.md
    - text: Definition language
      link: api/definition.md
      theme: alt
---
