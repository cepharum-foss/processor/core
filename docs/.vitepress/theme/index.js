import DefaultTheme from "vitepress/theme";
import TagOptional from "./components/TagOptional.vue";
import TagRequired from "./components/TagRequired.vue";
import PrefixTag from "./components/PrefixTag.vue";
import TypeString from "./components/TypeString.vue";
import TypeBoolean from "./components/TypeBoolean.vue";
import TypeNumber from "./components/TypeNumber.vue";
import TypeEnum from "./components/TypeEnum.vue";
import TypeKeyword from "./components/TypeKeyword.vue";
import TypeLocalizable from "./components/TypeLocalizable.vue";

export default {
	extends: DefaultTheme,
	enhanceApp( { app } ) {
		app.component( "TagOptional", TagOptional );
		app.component( "TagRequired", TagRequired );
		app.component( "PrefixTag", PrefixTag );
		app.component( "TypeString", TypeString );
		app.component( "TypeLocalizable", TypeLocalizable );
		app.component( "TypeBoolean", TypeBoolean );
		app.component( "TypeNumber", TypeNumber );
		app.component( "TypeEnum", TypeEnum );
		app.component( "TypeKeyword", TypeKeyword );
	}
};
