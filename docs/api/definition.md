# Definition reference

## Basic structure

A YAML-based forms definition consist of these sections:

- options
- forms
- processors

### Options

The `options` part of definition contains global options applying to the whole definition.

### Forms

The `forms` part is a list of definitions [each describing a single form](definition/forms.md).

### Processors

The `processors` part is defining one or more post-processing steps.

