# Definition of a form

In `forms` part of a definition, one or more forms are defined. For every form, these properties are supported:

## name

Every form requires a _case-insensitive unique_ name. This name is used to create an accordingly named section in resulting data structure.

## fields

The fields property is a list of [fields definitions](fields.md).
