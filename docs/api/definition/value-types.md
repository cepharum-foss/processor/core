# Supported value types

Different kinds of values are supported in a definition. This page is documenting specific types that imply certain constraints.

## string

Values of type `string` accept any single sequence of characters.

## keyword

Keywords are like strings, but some constraints apply.

* Leading and trailing whitespace is ignored.
* There must be no inner whitespace.
* Keywords start with a letter or an underscore.
* They may consist of letters, digits, dashes and underscores, only.

Keywords may be case-insensitive.

## enum

A value of type `enum` is one keyword out of a defined set of keywords.

## localizable

A value of type `localizable` is either a single string or a dictionary of strings with either entry's name representing a keyword matching some locale and its value providing a string to use in context of that locale. Names of dictionary entries are case-insensitive. 

The special name `any` is supported to mark a fallback variant to use if no other entry is matching current locale.

```yaml
label: localizable values
text:
  de: Dies ist der Text, welcher erscheint, wenn die aktuelle Sprache Deutsch ist.
  fr-ca: Il s'agit du texte qui apparaît lorsque la langue actuelle est le français canadien.
  uk: Це текст, який з'являється, коли поточною мовою є українська.
  any: This is the text that appears if current locale is neither German, French or Ukrainian.
```

In this example, both `label` and `text` are of type `localizable`. The former is illustrating option to use a simple string for every locale. The latter is providing a set of localized variants to pick from based on current locale.

## number

Without further constraints, a value of type `number` is a sequence of decimal digits without thousands separator optionally followed by a period as decimal separator and another sequence of decimal digits.

## boolean

The boolean value `true` can be represented by case-insensitive keywords:

* `true`
* `yes`
* `y`
* `on`

The boolean value `false` can be represented by case-insensitive keywords:

* `false`
* `no`
* `n`
* `off`
