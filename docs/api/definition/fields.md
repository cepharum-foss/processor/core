# Field definitions

Every field's definition is a set of properties. Some of them are commonly available for all fields. Some depend on field's type which is selected by its [`type`](#type) property.

This is a list of common properties supported by all kinds of fields:

## type <TagOptional/> <TypeEnum />

This property selects the type of field. Based on its value, additionally properties become available.

Supported types:

* [text](fields/text.md) (default)
* [group](fields/group.md)
* [date](fields/date.md)
* [upload](fields/upload.md)
* [download](fields/download.md)
* [static](fields/static.md)

## name <TagOptional/> <TypeKeyword/>

A field requires a _case-insensitive_ and _locally unique_ name to be used to name its value in a resulting data structure.

:::tip Locally unique
A locally unique name complies with this constraint:

* Two fields of a single form must not have the same name.

However, there is one exception:

* Two fields of a single form subordinated to different [groups](fields/group.md) may have the same name, though. In this, the form itself can be considered such a group, too.
:::

## label <TagOptional/> <TypeLocalizable/>

The label is displayed as part of the field. It is often rendered next to.

Labels are optional to support certain types of fields that usually don't require a label such as [`static`](fields/static.md) fields. Because of that, they don't have a default either.

## visible <TagOptional/> <TypeBoolean/>


